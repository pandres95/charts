{{/* vim: set filetype=mustache: */}}

{{/* Expand the name of the chart. */}}
{{- define "microservice.name" -}}
    {{- default .Values.name .Values.version | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "microservice.fullname" -}}
    {{- printf "%s-%s-%s" .Values.name .Values.version .Values.environment | replace "." "-" | trunc 63 | trimSuffix "-" -}}
{{- end -}}


{{/* Create chart name and version as used by the chart label. */}}
{{- define "microservice.chart" -}}
    {{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/* Common labels */}}
{{- define "microservice.labels" -}}
helm.sh/chart: {{ include "microservice.chart" . }}
{{- if .Values.version }}
app.kubernetes.io/version: {{ .Values.version | replace "v" "" }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{ include "microservice.selectorLabels" . }}
{{- end -}}


{{/* Selector labels */}}
{{- define "microservice.selectorLabels" -}}
app.kubernetes.io/name: {{ include "microservice.name" . }}
app.kubernetes.io/instance: {{ include "microservice.fullname" . }}
app.kubernetes.io/environment: {{ .Values.environment }}
{{- end -}}


{{/* Create the name of the service account to use */}}
{{- define "microservice.serviceAccountName" -}}
    {{- if .Values.serviceAccount.create -}}
        {{- default (include "microservice.fullname" .) .Values.serviceAccount.name -}}
    {{- else -}}
        {{- default "default" .Values.serviceAccount.name -}}
    {{- end -}}
{{- end -}}
